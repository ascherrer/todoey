//
//  Category.swift
//  Todoey
//
//  Created by Aymeric SCHERRER on 1/22/19.
//  Copyright © 2019 Aymeric SCHERRER. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var colour: String = ""
    let items = List<Item>()
}
